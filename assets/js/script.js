// Author:  Jacek Becela
// Source:  http://gist.github.com/399624
// License: MIT

    

jQuery(document).ready(function($) { 
    $('#block-holder').on( 'click', '.icon .remove',function(){
        $(this).closest('.icon').remove();
    });

    function doFormUpload() {
        var el = document.getElementById("image-form-field");
        if (el) {
            el.click();
        }
    }

    $('#block-holder').on( 'click', '.item', function(){
        doFormUpload();
    });

    function resizeImage( file, dataUri ) {
        var tempImage = new Image();
        tempImage.src = dataUri;

        tempImage.onload = function(){

            // Resize the image
            var canvas = document.createElement('canvas'),
                max_size = 768,// TODO : pull max size from a site config
                width = tempImage.width,
                height = tempImage.height;
            if (width > height) {
                if (width > max_size) {
                    height *= max_size / width;
                    width = max_size;
                }
            } else {
                if (height > max_size) {
                    width *= max_size / width;
                    height = max_size;
                }
            }
            canvas.width = width;
            canvas.height = height;
            canvas.getContext('2d').drawImage( tempImage, 0, 0, width, height);
            dataUri = canvas.toDataURL('image/png');

            fileLoaded( file.name, dataUri );
        }
    }
    var loader = document.getElementById('la-anim-6-loader')
        , border = document.getElementById('la-anim-6-border')
        , α = 0
        , π = Math.PI
        , t = 15
        
        , tdraw;

    function PieDraw() {
        α++;
        α %= 360;
        var r = ( α * π / 180 )
        , x = Math.sin( r ) * 250
        , y = Math.cos( r ) * - 250
        , mid = ( α > 180 ) ? 1 : 0
        , anim = 'M 0 0 v -250 A 250 250 1 ' 
               + mid + ' 1 ' 
               +  x  + ' ' 
               +  y  + ' z';

        loader.setAttribute( 'd', anim );
        border.setAttribute( 'd', anim );
        if( α != 0 )
        tdraw = setTimeout(PieDraw, t); // Redraw
    }

    function PieReset() {
        clearTimeout(tdraw);
        var anim = 'M 0 0 v -250 A 250 250 1 0 1 0 -250 z';
        loader.setAttribute( 'd', anim );
        border.setAttribute( 'd', anim );
    }

    function switchTab( type, btn ) {
        var current = $('#sidebar .tab.active');
        $('#btn-continue,#btn-return').removeClass('disabled');
        if ( type == 'next' ) {
            if ( (current.is('.frames') && ! $('#image-holder').data('uploaded')) ) {
                btn.addClass('disabled');
                return false;
            }
            if ( current.is('#sidebar .tab:last') ) {
                // create post upload
                html2canvas( $('#block-holder') , {
                    onrendered: function(canvas) {
                        var imageRendered = canvas.toDataURL('image/png');
                        //Save post
                        
                        

                        PieDraw(); jQuery('.la-anim-6').addClass('la-animate');
                        $.ajax({
                            url: dwmc.ajaxurl,
                            type: 'POST',
                            dataType: 'json',
                            data: {
                                action: 'rb_save_image',
                                imageSource: imageRendered,
                                message : $('#message').val(),
                                _wpnonce: $('#wpnonce').val()
                            },
                        }).done(function() {
                            $.colorbox({
                                html: "<div class=\"success-message\"><div class=\"inner\"><p>Cảm ơn bạn đã tham gia chương trình.</br> Kết quả của cuộc thi ảnh \"Mẹ làm điệu cho bé\" sẽ được công bố vào ngày 07/04/2015</p></div></div>",
                                title: false,
                                scalePhotos : false,
                                fixed: true,
                                onClosed: function(){
                                    window.location.href = dwmc.list_page;
                                }
                            });
                        })
                        .always(function() {
                            PieReset();
                            jQuery('.la-anim-6').removeClass('la-animate');
                        });
                    }
                });
                return false;
            }
            var next = current.next('.tab');
        } else {
            $('#btn-continue').css( 'background-image', 'url("' + dwmc.imageFolder + 'btn-cont.png")' );
            var next = current.prev('.tab');
            if ( current.is('#sidebar .tab:first') ) {
                btn.addClass('disabled');
                return false;
            }
        }
        current.removeClass('active');
        next.addClass('active');
        $('#progress-bar').css( 'background-image', 'url("' + dwmc.imageFolder + 'process-bar-' + $('#sidebar .tab').index( next ) + '.png")' );
        if ( next.is('#sidebar .tab:last') && type == 'next' ) {
            btn.css( 'background-image', 'url("' + dwmc.imageFolder + 'btn-finish.png")' );
        }
    }
    $('#btn-continue').on('click', function(event){
        event.preventDefault();
        switchTab('next', $(this) );
        return false;
    });
    $('#btn-return').on('click', function(event){
        event.preventDefault();
        switchTab('prev', $(this) );
        return false;
    });

    function draggableIcon() {
        $('#block-holder .icon').draggable({
            containment: "parent"
        }).resizable({
            aspectRatio: true,
            handles: 'se, sw, ne, nw'
        });
        $('#block-holder .icon').rotatable();
    }
    draggableIcon();

    $('#sidebar .icons .content').on('click',function(){
        var temp = $('<div class="icon" style="z-index:11"><span class="remove">x</span></div>');

        var tempImg = new Image();
        tempImg.src = $(this).find("img").attr("src");
        temp.append(tempImg);

        $('#block-holder').prepend( temp );
        draggableIcon();

    });
    $('.frames .content').click( function() {
        cloneItem($(this));
    });

    $('#sidebar .effects .content').on('click',function(){
        if ( $(this).find('canvas').length <= 0 ) {
            var source = $(this).find('img').attr('src');
        } else {
            var id = $(this).find('canvas').attr('id');
            var source = document.getElementById( id ).toDataURL();
        }
        $('#image-holder').attr('src', source );
    });

    $(document).keydown(function(event) {
        if (event.ctrlKey==true && (event.which == '107' || event.which == '109')) {
            event.preventDefault();
         }
    });

    window.addEventListener("dragover",function(e){
        e = e || event;
        e.preventDefault();
    },false);
    window.addEventListener("drop",function(e){
        e = e || event;
        e.preventDefault();
    },false);

    $(document).on( 'DOMMouseScroll mousewheel', function ( event ) {
        if (event.ctrlKey==true) return false;
    });
    
    function addEffect() {
        var effects = [ 'vintage', 'gray', 'lomo' ];
        for (var i = effects.length - 1; i >= 0; i--) {
            var id = '#caman-' + effects[i];
            switch ( effects[i] ) {
                case 'vintage':
                    Caman( id, function () {
                      this
                        .greyscale()
                        .contrast(5)
                        .noise(3)
                        .sepia(100)
                        .channels(8,2,4)
                        .gamma(0.87)
                        .vignette("40%", 30)
                        .render();
                    });
                    break;
                case 'gray':
                    Caman( id, function () {
                      this
                        .greyscale()
                        .render();
                    });
                    break;
                case 'lomo':
                    Caman( id, function () {
                      this
                        .contrast(10)
                        .sepia(60)
                        .saturation(-20)
                        .render();
                    });
                    break;
                default:
                    break;
            }

        };
    }

    function fileLoaded(filename, dataUri) {
        if (/^data:image/.test(dataUri)) {

            var tempImage = new Image();
            tempImage.src = dataUri;
            tempImage.onload = function() {

                $.colorbox({
                    html: "<img src=" + dataUri + " alt=" + filename + " />",
                    title: false,
                    scalePhotos : false,
                    maxWidth: $(window).width(),
                    maxHeight: $(window).height(),
                    fixed: true,
                    top: 0,
                    onComplete: function() {   
                        $('#cboxOverlay').prepend('<div id="dwmc-crop"><a href="#" class="btn btn-primary">Crop</a></div>');

                        var itemCropper = $('#cboxLoadedContent img');

                        var temp = new Image();
                        temp.src = itemCropper.attr("src");

                        itemCropper.cropper({
                            aspectRatio: 1,
                            zoomable: false,
                            dragCrop: false
                        });

                        $('#dwmc-crop').click( function () {
                            var imageCroped = itemCropper.cropper("getDataURL", "image/png");

                            $('#image-holder').attr("src", imageCroped);

                            var base64Image = imageCroped.replace('data:image/png;base64,', '');
                            $('#image-holder').data({
                                uploaded: true,
                                source : base64Image
                            });
                            $('#sidebar .effects .content').empty();
                            var effects = [ 'lomo', 'gray', 'vintage', 'normal' ];
                            var i = 0;
                            $('#sidebar .effects .content').each(function(index, el) {
                                $(this).append('<img id="caman-'+effects[i]+'" src="' + imageCroped + '">' );
                                i++;
                            });
                            addEffect();

                            $.colorbox.close();
                        });
                    }
                }); 
            }   
        } 
    };


    $(document).bind('cbox_closed', function(){
        $.colorbox.remove();
        $('#dwmc-crop').remove();
    });

    function uploadImage(target) {  
        target.ondragover = function () { target.className = 'hover'; return false; };
        target.ondragleave = function () { target.className = ''; return false; };
        target.ondragend = function () { target.className = ''; return false; };
        target.ondrop = function(e) {
            e.preventDefault();

            var file = e.dataTransfer.files[0],
                reader = new FileReader();

            target.className = '';

            if (!file.type.match('image')) {
                console.clear();
                return false;
            }

            reader.readAsDataURL(file);

            reader.onload = function(event) {
                var newImage = resizeImage( file, event.target.result );
            };

            return false;
        };
    }

    function cloneItem($item) {
        var image_holder = $('#image-holder');
        var data = image_holder.data();

        $('#block-holder').empty();

        $("#block-holder .item").removeClass('active');

        var temp = new Image();
        temp.src = $item.find("img").attr("src");

        var workspaceWidth = $('#block-holder').width();

        var clone = $item.clone();

        clone.appendTo('#block-holder').removeClass('content').addClass('item').css({
            "position": "absolute",
            "width": workspaceWidth,
            'z-index' : 10
        });
        // var data = $('#image-holder').data();
        // console.log( data );
        // if ( ! $('#image-holder').data('uploaded') ) {
        //     var image_holder = '<img id="image-holder" src="'+dwmc.default_image+'" />';
        // } else {
        //     var image_holder = $('#image-holder');
        // }   
            $('#block-holder').append(image_holder);

            $('#image-holder').css({
                'position': 'absolute',
                'left': 0,
                'top': 0,
                'width': 500,
                'height': 500,
                'pointer-events': null,
                'transform': null,
                'transform-origin': null
            }).data( data );
    }

    var dropper = document.getElementById('block-holder');

    uploadImage(dropper);
    
    $('html').keyup(function(e){
        if(e.keyCode == 13)
            $("#dwmc-crop").click();
    });

    $(document).on( 'dblclick', '.cropper-dragger', function(){
        $("#dwmc-crop").click();
    });

    function handleFileSelect(evt) {
        var files = evt.target.files; // FileList object
        var clone = $('#image-form').clone(true);
        $('#image-form').replaceWith(clone);
        // files is a FileList of File objects. List some properties.
        var output = [];
        for ( var i in files ) {
            var f = files[i];
            var fType = typeof f;
            if ( fType != 'object' ) {
                continue;
            }
            var reader = new FileReader();
            if (!f.type.match('image')) {
                console.clear();
                return false;
            }

            reader.readAsDataURL(f);

            reader.onload = function(event) {
                var newImage = resizeImage( f, event.target.result );
            };
        }
    }
    $(document).on( 'change', '#image-form', handleFileSelect );
});