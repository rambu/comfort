<?php  
/**
 * 	Plugin Name: RB Mockup Creator
 * 	Author: Rambu
 * 	Author URI: vn.nqhung@gmail.com
 * 	Description: Custom from DW Mockup Creator. For comfort project
 * 	Version: 1.0
 *  License: GPLv2
 */
if ( ! defined('RB_MOCKUP_FB_ID') ) {
	define( 'RB_MOCKUP_FB_ID', '312068372256054' );
}

function dwmc_activate() {
	
	// Auto create generator page
	$options = get_option( 'dwmc_options' );

	if ( ! isset( $options['pages']['mockup-creator-page-form'] ) || ( isset( $options['pages']['mockup-creator-page-form'] ) && ! get_page( $options['pages']['mockup-creator-page-form'] ) ) ) {
		$args = array(
			'post_title' => __( 'Mockup Creator', 'dw' ),
			'post_type' => 'page',
			'post_status' => 'publish',
			'post_content'  => '[dw-mockup-creator-form]',
		);
		$mockup_creator_page = get_page_by_path( sanitize_title( $args['post_title'] ) );
		if ( ! $mockup_creator_page || $mockup_creator_page->post_status != 'publish' ) {
			$options['pages']['mockup-creator-page-form'] = wp_insert_post( $args );
		} else {
			$options['pages']['mockup-creator-page-form'] = $mockup_creator_page->ID;
		} 
	}

	// Valid page content to ensure shortcode was inserted
	$mockup_creator_page_content = get_post_field( 'post_content', $options['pages']['mockup-creator-page-form'] );
	if ( strpos( $mockup_creator_page_content, '[dw-mockup-creator-form]' ) === false ) {
		$mockup_creator_page_content = str_replace( '[dw-mockup-creator-form]', '', $mockup_creator_page_content );
		wp_update_post( array(
			'ID'			=> $options['pages']['mockup-creator-page-form'],
			'post_content'	=> $mockup_creator_page_content . '[dw-mockup-creator-form]',
		) );
	}

	update_option( 'dwmc_options', $options );	
}
register_activation_hook( __FILE__, 'dwmc_activate' );

function dwmc_enqueue_scripts() {
	$options = get_option( 'dwmc_options' ); 
	if ( isset( $options['pages']['mockup-creator-page-form'] ) && is_page( $options['pages']['mockup-creator-page-form'] ) ) {
		wp_enqueue_script('jquery-ui-core');
		wp_enqueue_script('jquery-ui-widget'); 	 
		wp_enqueue_script('jquery-ui-mouse');
		wp_enqueue_script('jquery-ui-resizable');
		wp_enqueue_script('jquery-ui-draggable');
		wp_enqueue_script('jquery-ui-droppable');
		wp_enqueue_script('dw-mockup-creator-cropper-js', plugins_url('assets/js/cropper.js', __FILE__), array('jquery'));
		wp_enqueue_script('dw-mockup-creator-colorbox-js', plugins_url('assets/js/jquery.colorbox-min.js', __FILE__), array('jquery'));
		wp_enqueue_script('dw-mockup-creator-canvas', plugins_url('assets/js/html2canvas.js', __FILE__), array('jquery'));

		wp_enqueue_script( 'camanjs', 'http://cdnjs.cloudflare.com/ajax/libs/camanjs/4.0.0/caman.full.min.js', array('jquery') );
		wp_enqueue_script('rotate', plugins_url('assets/js/jquery.ui.rotatable.min.js', __FILE__), array('jquery', 'jquery-ui-core', 'jquery-ui-widget') ) ; 

		wp_enqueue_script('dw-mockup-creator-script', plugins_url('assets/js/script.js', __FILE__), array('jquery')); 
		wp_localize_script( 'dw-mockup-creator-script', 'dwmc', array(
			'ajaxurl' => admin_url( 'admin-ajax.php' ),
			'default_frame' => plugins_url('assets/img/frame_comfort_default.png', __FILE__),
			'default_image' => plugins_url('assets/img/default_image.png', __FILE__),
			'imageFolder' => plugins_url('assets/img/', __FILE__),
			'list_page' => get_page_link( $options['pages']['mockup-creator-page-form'] ),
		) );


		wp_enqueue_style('dw-mockup-creator-cropper-style', plugins_url('assets/css/cropper.css', __FILE__));
		wp_enqueue_style('dw-mockup-creator-colorbox-style', plugins_url('assets/css/colorbox.css', __FILE__));
		wp_enqueue_style('normalize', plugins_url('assets/css/normalize.css', __FILE__));
		wp_enqueue_style('dw-mockup-creator-style', plugins_url('assets/css/style.css', __FILE__));
	}
}
add_action( 'wp_enqueue_scripts', 'dwmc_enqueue_scripts' );

add_action( 'wp_footer', 'dwmc_page_loading_effect' );
function dwmc_page_loading_effect() {
	?>
	<div class="la-anim-6">
		<svg xmlns="http://www.w3.org/2000/svg" version="1.1" width="500" height="500" viewbox="0 0 500 500">
		  <path id="la-anim-6-border" transform="translate(250, 250)"/>
		  <path id="la-anim-6-loader" transform="translate(250, 250) scale(0.9)"/>
		</svg>
	</div>
	<?php
}

function dwmc_generator_shortcode() {
	$options = get_option( 'dwmc_options' );
	$page = isset($options['pages']['mockup-creator-page-form']) ? $options['pages']['mockup-creator-page-form'] : false;
	if ( isset($_REQUEST['image']) ) {
		$image = get_post( intval( $_REQUEST['image'] ) );
		if ( ! is_wp_error( $image ) ) {
		?>
		<div id="image-tool-page">
			<div id="workspace">
				<div id="image">
					<img src="<?php echo get_post_meta( $image->ID, '_rb_image_base64', true ); ?>">
				</div>
				<div class="fb-like">
					<div class="fb-like" data-href="<?php echo add_query_arg( 'image', $image->ID, get_permalink( $page ) ); ?>" data-layout="standard" data-action="like" data-show-faces="true" data-share="true"></div>
				</div>
			</div>
			<div id="sidebar">
				<div class="message">
					<div class="title fnt clCya titTxtMys"><?php
						$user = get_userdata( $image->post_author );
						echo $user->display_name ? $user->display_name : '&nbsp;';
					?></div>
					<?php echo apply_filters( 'the_content', $image->post_content ); ?>
					<br>
					<div class="fb-comments" data-href="<?php echo add_query_arg( 'image', $image->ID, get_permalink() ); ?>" data-width="450px" data-numposts="5" data-colorscheme="light" style="min-height:500px;"></div>
				</div>
			</div>
		</div>
		<?php
		}
	} elseif ( isset($_REQUEST['type']) && $_REQUEST['type'] == 'design' ) {
	?>

	<div id="image-tool-page">
	  	<div id="topbar"><div class="pull-right"></div></div>
  		<div class="progress-bar-box">
  			<div id="progress-bar"></div>
  		</div>
		<div class="clear"></div>
	  	<div id="workspace">
	    	<div id="block-holder">
		    	<div class="item">
		    		<img src="<?php echo plugins_url('assets/img/frame_comfort_default.png', __FILE__) ?>" alt="Default">
		    	</div>
	    		<img id="image-holder" src="<?php echo plugins_url('assets/img/default_image.png', __FILE__); ?>" alt="Default">
	    	</div>
	    	<form id="image-form">
		   		<input type="file" name="image-form-field" id="image-form-field" accept="image/*" multiple="" >
		   	</form>
  		</div>
	  	<div id="sidebar">
	  		<?php $frames_url = plugin_dir_url(__FILE__) . 'assets/img/'; ?>
	  		<div class="tab active frames">
		  		<table>
		  			<tr>
		  				<td>
		  					<div class="content"><img src="<?php echo $frames_url . 'frame_comfort_default.png' ?>" alt="Frame Default"/></div>
		  				</td>
		  				<td>
		  					<div class="content"><img src="<?php echo $frames_url . 'frame_comfort_1.png' ?>" alt="Frame 1"/></div>
		  				</td>
		  			</tr>
		  			<tr>
		  				<td>
		  					<div class="content"><img src="<?php echo $frames_url . 'frame_comfort_2.png' ?>" alt="Frame 2"/></div>	
		  				</td>
		  				<td>
		  					<div class="content"><img src="<?php echo $frames_url . 'frame_comfort_3.png' ?>" alt="Frame 3"/></div>
		  				</td>
		  			</tr>
		  		</table>
	  		</div>
		  	<div class="tab effects">
		  		<table>
		  			<tr>
		  				<td>
		  					<div class="content effect"><img id="caman-lomo" src="#" alt="Effect 1"/></div>
		  				</td>
		  				<td>
		  					<div class="content effect"><img id="caman-gray" src="#" alt="Effect Gray"/></div>
		  				</td>
		  			</tr>
		  			<tr>
		  				<td>
		  					<div class="content effect"><img id="caman-vintage" src="#" alt="Effect Vintage"/></div>	
		  				</td>
		  				<td>
		  					<div class="content  effect"><img id="caman-normal" src="#" alt="Effect Normal"/></div>
		  				</td>
		  			</tr>
		  		</table>
		  	</div>
			<div class="tab icons">
				<table>
					<tr>
					<?php for ($i=0; $i < 12; $i++) { ?>
						<?php if ( $i % 4 == 0 ) { ?>
					</tr>
					<tr>
						<?php } ?>
						<td>
							<div class="content">
								<img src="<?php echo $frames_url . 'icon-'.($i+1).'.png' ?>" alt="Icon"/>
							</div>
						</td>  
					<?php } ?>
					</tr>
					<tr>
						<td colspan="4" style="padding:10px">
							<span id="message-label"></span>
							<?php wp_nonce_field( '_rb_save_images', 'wpnonce' ); ?>
							<textarea name="message" id="message" rows="7"></textarea>
						</td>
					</tr>
				</table>
			</div>
	  	</div>
	  	<div class="clear"></div>
	  	<div id="footer">
	  		<a id="btn-return" href="#backstep" alt="back step"><?php _e('Quay lại') ?></a>
	  		<a id="btn-continue" href="#nextstep" alt="next step"><?php _e('Tiếp tục') ?></a>
	  	</div>
		</div>
	</div>
	<?php } else { ?>
	<div id="image-tool-page">
		<div id="images-listing">
			<?php  
			if ( isset($_GET['page']) ) {
				$paged = intval( $_GET['page'] );
			}
			if ( ! $paged ) {
				$paged = get_query_var( 'paged' );
				$paged = $paged ? $paged : 1;
			}
			$images = get_posts( array(
				'post_type' => 'rb_content_image',
				'posts_per_page' => 6,
				'paged' => $paged
			) );
			$total = wp_count_posts( 'rb_content_image' );

			if ( !empty( $images ) ) :
			foreach ($images as $image) :
				$link = add_query_arg( 'image', $image->ID, get_permalink( $page ) );
			?>
			<div class="rb-col-4">
				<div class="inner">
					<div class="image"><a href="<?php echo $link; ?>" ?><img src="<?php echo get_post_meta( $image->ID, '_rb_image_base64', true ); ?>" alt="<?php echo get_the_title( $image->ID ); ?>"></a></div>
					<p class="title fnt clCya"><?php
						$user = get_userdata( $image->post_author );
						echo $user->display_name ? $user->display_name : '&nbsp;';
					?></p>
					<div class="fb-like" data-href="<?php echo $link; ?>" data-layout="button_count" data-action="like" data-show-faces="true" data-share="true"></div>
				</div>
			</div>
			<?php
			endforeach;
			endif;

		?>
		</div>
	  	<div class="clear"></div>
	  	<?php  
	  		$options = get_option( 'dwmc_options' );
	  		$page = isset($options['pages']['mockup-creator-page-form']) ? $options['pages']['mockup-creator-page-form'] : false;
	  		if ( $page ) {
	  	?>
	  	<div id="footer">
	  		<?php
	  			$total = $total->publish;
			$pageNumber = ceil( $total / 6 );
			if ( $pageNumber > 1 ) {
				$pageLink = get_page_link( $page );

		?>
			<div class="wp-pagenavi">
				<?php 
				if ( $paged > 1 ) {
					echo '<a class="previouspostslink" rel="prev" href="'.add_query_arg( 'paged', $i-1, $pageLink ).'"></a>';
				}
				for( $i = 1; $i <= $pageNumber; $i++ ) {
					if ( $paged == $i ) {
						echo '<span class="current">'.$i.'</span>';
					} elseif ( $pageNumber >= 50 ) {
						$mid = ceil( $pageNumber / 2 );
						if (  ( $i > 3 && $i < $mid - 1 ) || ( $i > $mid + 1 && $i < $pageNumber - 3 ) ) {
							continue;
						} else {
							$link = add_query_arg( 'paged', $i, $pageLink );
							echo '<a class="page larger" href="' . $link .'">' . $i . '</a>';
						}
					} else {
						$link = add_query_arg( 'paged', $i, $pageLink );
						echo '<a class="page larger" href="' . $link .'">' . $i . '</a>';
					}
				} 
				if ( $paged < $pageNumber ) {
					echo '<a class="nextpostslink" rel="next" href="'.add_query_arg( 'paged', $i+1, $pageLink ).'"></a>';
				}
				?>
			</div>
		<?php } ?>
			<div class="clear"></div>
	  		<a id="btn-join" href="<?php echo add_query_arg( 'type', 'design', get_permalink( $page ) ); ?>" alt="Join"><?php _e('Tham gia ngay') ?></a>
	  	</div>
	  	<?php } ?>
	</div>
	<?php
	}
}
add_shortcode('dw-mockup-creator-form', 'dwmc_generator_shortcode');

function dw_mockup_creator_load_content_image(){

	$plugin_dir = plugin_dir_path(__FILE__).'assets/img/';

	if ( $handle = opendir($plugin_dir) ) {

	    /* This is the correct way to loop over the directory. */
	    while (false !== ($entry = readdir($handle))) {
	    	if( is_file($plugin_dir.$entry) ){

				extract( dw_mockup_creator_content_image_info($entry) );
				if($image_ext !='png') continue; 

    			?>
    			 	<div class="content"><img src="<?php echo $url ?>" alt="<?php echo $label; ?>"/></div>		
    			<?php 
	    	}
	    }

	    closedir($handle);
	}
}

function dw_mockup_creator_content_image_info($entry){
	$plugin_url = plugin_dir_url(__FILE__).'assets/img/';
	$plugin_dir = plugin_dir_path(__FILE__).'assets/img/';

	$image_basename = pathinfo($entry, PATHINFO_BASENAME);
	$image_filename = pathinfo($entry, PATHINFO_FILENAME);
    $image_ext = pathinfo($entry,PATHINFO_EXTENSION);
	$image_url = $plugin_url.$image_basename; 
	if( is_file($plugin_dir.$entry) ){
		return array(
			'label' 	=>  $image_filename,
			'url'		=>	$image_url,
			'image_ext' => 	$image_ext
		);
	}
}

add_action('wp_ajax_nopriv_dw-get-image-src', 'dw_mockup_creator_save_image_transformed');
add_action('wp_ajax_dw-get-image-src', 'dw_mockup_creator_save_image_transformed');

function dw_mockup_creator_save_image_transformed() {

	if ( !isset($_REQUEST['source']) || !isset($_REQUEST['filename']) ) wp_send_json_error();

	$image_source = base64_decode($_REQUEST['source']);

	$currentFile = $_REQUEST['filename'];

	$xml = simplexml_load_file(plugin_dir_url(__FILE__)."mockup-control-point.xml");

	foreach($xml->children() as $mockup) {
		if ( $mockup['name'] == $currentFile ) {
			$controlPoints = explode("," ,$mockup->value);

			$leftimgk = intval($mockup->left);
			$topimgk = intval($mockup->top);
			$widthimgk = intval($mockup->width);
			$heightimgk = intval($mockup->height);
		}
	}

	if ( $controlPoints == "null" || $widthimgk == "null" || $heightimgk == "null" ) wp_send_json_error();

	if ( $leftimgk == "null" ) $leftimgk = 0;
	if ( $topimgk == "null" ) $topimgk = 0;

	try {

		$imgk = new Imagick();

		$imgk->readImageBlob($image_source);

		$imgk->resizeImage($widthimgk,$heightimgk,Imagick::FILTER_UNDEFINED ,0.5);

		$imgk->setImageFormat('png');

		$imgk->setImageVirtualPixelMethod(Imagick::VIRTUALPIXELMETHOD_TRANSPARENT);

		$imgk->setImageMatte(true);
		
		$imgk->distortImage(Imagick::DISTORTION_PERSPECTIVE, $controlPoints, true);

		ob_start();
		$thumbnail = $imgk->getImageBlob();
		$contents =  ob_get_contents();
		ob_end_clean();

		$data = array( 
			"src" => base64_encode($thumbnail),
			"left" => $leftimgk,
			"top" => $topimgk,
			"width" => $widthimgk,
			"height" => $heightimgk
		);

		wp_send_json_success($data);

	} catch (Exception $e) {
		echo $e->getMessage();
	}
}

/**
 * Registers a new post type
 * @uses $wp_post_types Inserts new post type object into the list
 *
 * @param string  Post type key, must not exceed 20 characters
 * @param array|string  See optional args description above.
 * @return object|WP_Error the registered post type object, or an error object
 */
 function rb_mockup_posttype() {
 
 	$labels = array(
 		'name'                => __( 'Contest Image', 'dwmc' ),
 		'singular_name'       => __( 'Contest Image', 'dwmc' ),
 		'add_new'             => _x( 'Add New Contest Image', 'dwmc', 'dwmc' ),
 		'add_new_item'        => __( 'Add New Contest Image', 'dwmc' ),
 		'edit_item'           => __( 'Edit Contest Image', 'dwmc' ),
 		'new_item'            => __( 'New Contest Image', 'dwmc' ),
 		'view_item'           => __( 'View Contest Image', 'dwmc' ),
 		'search_items'        => __( 'Search Contest Image', 'dwmc' ),
 		'not_found'           => __( 'No Contest Image found', 'dwmc' ),
 		'not_found_in_trash'  => __( 'No Contest Image found in Trash', 'dwmc' ),
 		'parent_item_colon'   => __( 'Parent Contest Image:', 'dwmc' ),
 		'menu_name'           => __( 'Contest Image', 'dwmc' ),
 	);
 
 	$args = array(
 		'labels'                   => $labels,
 		'hierarchical'        => false,
 		'description'         => 'description',
 		'taxonomies'          => array(),
 		'public'              => true,
 		'show_ui'             => true,
 		'show_in_menu'        => true,
 		'show_in_admin_bar'   => true,
 		'menu_position'       => null,
 		'menu_icon'           => null,
 		'show_in_nav_menus'   => true,
 		'publicly_queryable'  => true,
 		'exclude_from_search' => false,
 		'has_archive'         => true,
 		'query_var'           => true,
 		'can_export'          => true,
 		'rewrite'             => true,
 		'capability_type'     => 'post',
 		'supports'            => array(
 			'title', 'author', 'comments'
 		)
 	);
 
 	register_post_type( 'rb_content_image', $args );
 }
 add_action( 'init', 'rb_mockup_posttype' );

function rb_ajax_save_images() {
	check_ajax_referer( '_rb_save_images' );

	if ( ! isset($_POST['imageSource'] ) ) {
		wp_send_json_error( array( 'Image is not valid' ) );
	}
	$page = url_to_postid( $_SERVER["HTTP_REFERER"] );
	$args = array(
		'post_content' => isset($_POST['message']) ? esc_html( $_POST['message'] ) : '',
		'post_title' => '(no title)',
		'post_type' => 'rb_content_image',
		'post_status' => 'publish'
	);
	if ( is_user_logged_in() ) {
		global $current_user;
		$args['post_author'] = $current_user->ID;
	}
	$post_id = wp_insert_post( $args );
	if ( ! is_wp_error( $post_id ) ) {
		update_post_meta( $post_id, '_rb_image_base64', $_POST['imageSource'] );
		// wp_update_post( array(
		// 	'ID' => $post_id,
		// 	'post_title' => $post_id
		// ) );
		wp_send_json_success( array(
			'post' => $post_id,
			'link' => add_query_arg( 'image', $post_id, get_permalink( $page ) ),
			'listing_page' => get_permalink( $page ),
		) );
	} else {
		wp_send_json_error( array(
			'message' => $post_id->get_error_message()
		) );
	}
}
add_action( 'wp_ajax_rb_save_image', 'rb_ajax_save_images' );
add_action( 'wp_ajax_nopriv_rb_save_image', 'rb_ajax_save_images' );

function rb_wp_footer(){
	?>
	<div id="fb-root"></div>
	<script>
	(function(d, s, id) {
		var appId = '<?php echo RB_MOCKUP_FB_ID; ?>';
	  	var js, fjs = d.getElementsByTagName(s)[0];
	  	if (d.getElementById(id)) return;
	  	js = d.createElement(s); js.id = id;
	  	js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId="+appId+"&version=v2.0";
	  	fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
	</script>
	<?php
}
add_action( 'wp_footer', 'rb_wp_footer' );

add_action( 'wp_head', 'rb_facebook_opengraph' );
function rb_facebook_opengraph() {
	$options = get_option( 'dwmc_options' );
	$page = isset($options['pages']['mockup-creator-page-form']) ? $options['pages']['mockup-creator-page-form'] : false;
	if ( $page && is_page( $page ) && isset( $_GET['image'] ) ) {
		$image = get_post( intval( $_GET['image'] ) );
		if ( ! is_wp_error( $image ) ) {
			$link = add_query_arg( 'image', $image->ID, get_permalink($page) );
		?>
		<link rel='canonical' href='<?php echo $link; ?>' />
		<meta property="og:url" content="<?php echo $link; ?>" >
		<meta property="og:image" content="<?php echo admin_url( 'admin-ajax.php?action=rb_og_image&image=' . $image->ID ); ?>" >
		<meta property="og:description" content="<?php echo $image->post_content ?>" ?>
		<?php
		}
	}
}

add_action( 'wp_ajax_rb_og_image', 'rb_og_decode_image' );
add_action( 'wp_ajax_nopriv_rb_og_image', 'rb_og_decode_image' );
function rb_og_decode_image(){
	if ( ! isset( $_GET['image'] ) ) {
	    die;
	}
	$image_id = intval( $_GET['image'] );
	$image64 = get_post_meta( $image_id, '_rb_image_base64', true );
	if ( ! $image64 )  {
		die;
	}
	if (!preg_match('/data:([^;]*);base64,(.*)/', $image64, $matches)) {
	    die("error");
	}

	// Decode the data
	$content = base64_decode($matches[2]);

	// Output the correct HTTP headers (may add more if you require them)
	header('Content-Type: '.$matches[1]);
	header('Content-Length: '.strlen($content));

	// Output the actual image data
	echo $content;
}

function get_fb_likes($url)
{
  $query = "select total_count,like_count,comment_count,share_count,click_count from link_stat where url='{$url}'";
   $call = "https://api.facebook.com/method/fql.query?query=" . rawurlencode($query) . "&format=json";

  $ch = curl_init();
   curl_setopt($ch, CURLOPT_URL, $call);
   curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
   $output = curl_exec($ch);
   curl_close($ch);
   return json_decode($output);
}

/**
 * ADD Custom Meta Box
 */

function rb_meta_boxes() {

	$screens = array( 'rb_content_image' );

	foreach ( $screens as $screen ) {

		add_meta_box(
			'rb_mockup_contest',
			__( 'Contest', 'dwmc' ),
			'rb_meta_box_callback',
			$screen,
			'normal',
			'high'
		);
	}
}
add_action( 'add_meta_boxes', 'rb_meta_boxes' );

/**
 * Prints the box content.
 * 
 * @param WP_Post $post The object for the current post/page.
 */
function rb_meta_box_callback( $post ) {
	$options = get_option( 'dwmc_options' );
	$page = isset($options['pages']['mockup-creator-page-form']) ? $options['pages']['mockup-creator-page-form'] : false;
	$image = get_post_meta( $post->ID, '_rb_image_base64', true );
	$link = add_query_arg( 'image', $post->ID, get_permalink($page) );
	$point = get_post_meta( $post->ID, '_rb_image_point', true );
	$point = $point && is_numeric( $point ) ? $point : 0;
	?>
	<table class="table">
		<tr>
			<td>
				<img src="<?php echo $image ?>" alt="<?php echo $post->post_title ?>" style="width:100%">
			</td>
			<td>
				<table>
					<tr>
						<th>ID</td>
						<td><?php echo $post->ID ?></td>
					</tr>
					<tr>
						<th>Facebook Like</td>
						<td><?php echo rb_get_facebook_like_count( $link ); ?></td>
					</tr>
					<tr>
						<th>Message</td>
						<td><?php echo apply_filters( 'the_content', $post->post_content ); ?></td>
					</tr>
					<tr>
						<th>Điểm</td>
						<td><input type="number" name="rb_image_point" id="rb-image-point" value="<?php echo $point; ?>"></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<style type="text/css">
	#rb_mockup_contest table {
		width: 100%;
	}
		#rb_mockup_contest table th {
			text-align: left;
		}
		#rb_mockup_contest table td { 
			width: 50%;
			vertical-align: top;
		}
			#rb_mockup_contest table td img {
				outline: none;
			}

			#rb_mockup_contest table td input[type="number"] {
				width: 28px;
				line-height: 28px;
				max-width: 100%;
			}
	</style>
	<?php
}

function rb_get_facebook_like_count( $url ) {
	$like_count_url = 'https://graph.facebook.com/fql?q=select%20like_count,%20share_count%20%20from%20link_stat%20where%20url=%22'.urlencode($url).'%22';
	$result = wp_remote_get( $like_count_url );
	$resultBody = json_decode( wp_remote_retrieve_body( $result ) );
	//var_dump( $resultBody );

	if ( isset( $resultBody->data->like_count ) ) {
		return $resultBody->data->like_count;
	}
	return 0;
}

add_action( 'save_post', 'rb_save_meta_box' );
function rb_save_meta_box( $post_id ) {
	
	/*
	 * We need to verify this came from the our screen and with proper authorization,
	 * because save_post can be triggered at other times.
	 */

	// If this is an autosave, our form has not been submitted,
            //     so we don't want to do anything.
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) 
		return $post_id;

	// Check the user's permissions.
	if ( 'rb_content_image' == $_POST['post_type'] ) {

		if ( ! current_user_can( 'edit_page', $post_id ) )
			return $post_id;

	} else {

		if ( ! current_user_can( 'edit_post', $post_id ) )
			return $post_id;
	}

	/* OK, its safe for us to save the data now. */

	// Sanitize the user input.
	$mydata = sanitize_text_field( $_POST['rb_image_point'] );

	// Update the meta field.
	update_post_meta( $post_id, '_rb_image_point', $mydata );
}
